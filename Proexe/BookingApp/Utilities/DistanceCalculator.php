<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator {

	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return mixed
	 */

     function calculate($from, $to, $unit = km) {

        $theta = $from[0] - $from[1];
        $dist = sin(deg2rad($to[0])) * sin(deg2rad($to[1])) +  cos(deg2rad($to[0])) * cos(deg2rad($to[1])) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "km") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

	/**
	 * @param array $from
	 * @param array $offices
	 *
	 * @return array
	 */
	public function findClosestOffice( $from, $offices ) {
        $items = array();

        foreach ($offices as $office){
	        $data = $this->calculate(
                [ $from[0], $from[1] ],
                [ $office['lat'], $office['lng'] ],
                'km' );
        $items[] = $data;
        }
        print_r(min($items));
    }
}
